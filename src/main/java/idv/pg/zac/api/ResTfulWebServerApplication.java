package idv.pg.zac.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import idv.pg.zac.api.model.Car;

@SpringBootApplication
public class ResTfulWebServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResTfulWebServerApplication.class, args);
	}

}
