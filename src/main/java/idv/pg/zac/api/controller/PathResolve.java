package idv.pg.zac.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import idv.pg.zac.api.model.Brand;
import idv.pg.zac.api.model.Car;
import idv.pg.zac.api.model.CarType;

@RestController
public class PathResolve {

	private List<Car> cars = new ArrayList<Car>();
	
	public PathResolve() {
		build();
	}
	
	private void build() {
		Car c = new Car(627846, "A1 Sportback", Brand.AUDI, 1790000.0, CarType.GENERIC);
		cars.add(c);
		
		c = new Car(43267, "Odyssey", Brand.HONDA, 1688000.0, CarType.MPV);
		cars.add(c);
		
		c = new Car(41278, "2-Series Active Tourer", Brand.BMW, 1800000.0, CarType.WAGON);
		cars.add(c);
		
		c = new Car(41329, "QX60", Brand.INFINITI, 2850000.0, CarType.SUV);
		cars.add(c);
	}
	
	@GetMapping(value = "/user/xml", produces = MediaType.APPLICATION_XML_VALUE)
	public List<Car> listUserXML() {
		return cars;
	}
	
	@GetMapping(value = "/user/json", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Car> listUserJSON() {
		return cars;
	}
	
	
	
}
