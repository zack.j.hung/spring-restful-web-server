package idv.pg.zac.api.model;

public enum CarType {
	GENERIC("轎車"),
	SUV("SUV"),
	MPV("MPV"),
	WAGON("旅行車");
	
	private String type;
	
	CarType(String s) {
		type = s;
	}
	
	public String toString() {
		return type;
	}
}
