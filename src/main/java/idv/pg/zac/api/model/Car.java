package idv.pg.zac.api.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Getter @Setter
@Component
@Scope("prototype")
//@XmlRootElement
public class Car {
	private Integer serialNumber;
	private String name;
	private Brand brand;
	private Double price;
	private CarType type;
	
	
}
