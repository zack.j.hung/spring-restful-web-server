package idv.pg.zac.api.model;

public enum Brand {
	AUDI("audi"),
	BMW("BMW"),
	HONDA("Honda"),
	INFINITI("Infiniti");
	
	private String brand;
	
	Brand(String s) {
		brand = s;
	}
	
	public String toString() {
		return brand;
	}
}